
var socket = new WebSocket('ws://192.168.178.64:8082/');
socket.onopen = function(event) {
  console.log('Opened connection');
}

socket.onerror = function(event) {
  console.log('Error: ' + JSON.stringify(event));
}

socket.onmessage = function (event) {
  console.log(event.data);
  if(event.data == 'pause video') {
    pausePlaying();
  } else if(event.data == "continue playing") {
    continuePlaying();
  } else {
    changeSourceTo(event.data)
  }
}

socket.onclose = function(event) {
  console.log('Closed connection');
}

window.addEventListener('beforeunload', function() {
  socket.close();
});

// Change the source on video player
function changeSourceTo(source) {
    var player = document.getElementById('videoPlayer');
    var mp4Vid = document.getElementById('mp4Source');
    player.pause();
    console.console.log(source);
    mp4Vid.src = source;
    player.load();
    player.play();
    player.webkitRequestFullscreen()
  }

  function pausePlaying() {
    var player = document.getElementById('videoPlayer');
    player.pause();
  }

  function continuePlaying() {
    var player = document.getElementById('videoPlayer');
    player.play();
  }
